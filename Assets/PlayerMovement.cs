﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public float speed;
    public FloatingJoystick myJs;
    public CharacterController controller;

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        float z = myJs.Horizontal;
        float x = myJs.Vertical;

        Vector3 move = new Vector3(z,0,x);
        controller.Move(move * speed *  Time.deltaTime);
    }

    /*
    public void FixedUpdate() {
        Vector3 direction = Vector3.forward * myJs.Vertical + Vector3.right * myJs.Horizontal;
        controller.Move(direction * speed * Time.deltaTime);
    }*/
}
